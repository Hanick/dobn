﻿namespace DOBN.business.Filter
{
    public class PaginationFilter
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public string SortBy { get; set; }
        public bool IsAscending { get; set; }

        public PaginationFilter(int pageNumber, int pageSize)
        {
            //the minimum page number is always set to 1
            PageNumber = pageNumber < 1 ? 1 : pageNumber;

            //maximum page size a user can request is 10
            PageSize = pageSize > 10 ? 10 : pageSize;
        }
    }
}