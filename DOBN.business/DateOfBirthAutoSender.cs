﻿using DOBN.business.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DOBN
{
    public class DateOfBirthAutoSender : IHostedService, IDisposable
    {
        private readonly IServiceProvider _serviceProvider;
        private Timer _timer;

        public DateOfBirthAutoSender(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }


        public Task StartAsync(CancellationToken cancellationToken)
        {
            //Email message will be sent every day
            _timer = new Timer(AutoSender, null, TimeSpan.Zero,
            TimeSpan.FromSeconds(86400));
            return Task.CompletedTask;
        }

        void AutoSender(object state)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var _mailService = scope.ServiceProvider.GetRequiredService<IMailService>();
                _mailService.SendEmailAsync();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
