﻿using DOBN.business.Filter;
using DOBN.business.Services;
using DOBN.sharing.DTOs.Wrappers;
using System;
using System.Collections.Generic;

namespace DOBN.business.Helpers
{
    public static class PaginationHelper
    {
        public static PagedResponse<List<T>> CreatePagedReponse<T>(this List<T> pagedData, PaginationFilter validFilter, int totalRecords, IUriService uriService, string route)
        {
            //Takes the paged data from EfRepository
            var respose = new PagedResponse<List<T>>(pagedData, validFilter.PageNumber, validFilter.PageSize);

            //Initializes the Response Object with required params
            var totalPages = totalRecords / (double)validFilter.PageSize;

            //Initializes the Response Object with required params
            int roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));

            //Calculates the total pages
            respose.NextPage =
                validFilter.PageNumber >= 1 && validFilter.PageNumber < roundedTotalPages
                //Check if the requested page number is less than the total pages and generate the URI
                ? uriService.GetPageUri(new PaginationFilter(validFilter.PageNumber + 1, validFilter.PageSize), route)
                : null;
            respose.PreviousPage =
                validFilter.PageNumber - 1 >= 1 && validFilter.PageNumber <= roundedTotalPages
                ? uriService.GetPageUri(new PaginationFilter(validFilter.PageNumber - 1, validFilter.PageSize), route)
                : null;

            //Generates URLs for the First and Last page
            respose.FirstPage = uriService.GetPageUri(new PaginationFilter(1, validFilter.PageSize), route);
            respose.LastPage = uriService.GetPageUri(new PaginationFilter(roundedTotalPages, validFilter.PageSize), route);

            //Setting the total page and total records to the response object
            respose.TotalPages = roundedTotalPages;
            respose.TotalRecords = totalRecords;

            return respose;
        }
    }
}
