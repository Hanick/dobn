﻿using DOBN.sharing.DTOs.Response;
using System.Collections.Generic;

namespace DOBN.business.Services
{
    public interface IBirthDateService
    {
        List<BirthDateResponse> GetUsersByDate(int frommonth, int tomonth);
    }
}