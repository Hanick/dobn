﻿using DOBN.business.Filter;
using DOBN.data.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DOBN.business.Services.Impl
{
    public interface IPagingService
    {
        Task<List<User>> PagedData(PaginationFilter filter);
        Task<int> TotalRecords();
        PaginationFilter ValidFilter(PaginationFilter filter);
    }
}