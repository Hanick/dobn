﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DOBN.data.Entities;
using DOBN.sharing.DTOs.Models;
using DOBN.sharing.DTOs.Request;
using DOBN.sharing.DTOs.Response;

namespace DOBN.business.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        Task<AuthenticateResponse> Register(UserModel userModel);
        SubscriptionResponse GetSubscribed(SubscriptionRequest subscriptionRequest);
        IEnumerable<User> GetAll();
        User GetById(int id);
    }
}