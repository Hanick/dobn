﻿using System.Threading.Tasks;

namespace DOBN.business.Services
{
    public interface IMailService
    {
        Task SendEmailAsync();
    }
}