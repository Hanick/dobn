﻿using DOBN.business.Filter;
using DOBN.data.Entities;
using System.Linq;

namespace DOBN.business.Services
{
    public interface ISortingService
    {
        IQueryable<User> SortingData(PaginationFilter filter);
    }
}