﻿using DOBN.data.Entities;
using DOBN.data.Repositories;
using DOBN.business.Settings;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DOBN.business.Services.Impl
{
    public class MailService : IMailService
    {
        private readonly MailSettings _mailSettings;
        private readonly IBirthDateService _ibirthDateService;
        private readonly IUserRepository _userRepository;
        public MailService(IOptions<MailSettings> mailSettings, 
            IBirthDateService ibirthDateService, 
            IUserRepository userRepository)
        {
            _mailSettings = mailSettings.Value;
            _ibirthDateService = ibirthDateService;
            _userRepository = userRepository;
        }

        public async Task SendEmailAsync()
        {
            //Creates a new object of MimeMessage and adds in the Sender, To Address and Subject to this object. We will be filling the message related data (subject, body) from the mailRequest and the data we get from our JSON File.
            var email = new MimeMessage();
            email.Sender = MailboxAddress.Parse(_mailSettings.Mail);

            var user = _userRepository.GetAll().Where(x => x.IsSubscribed == true).ToList();

            foreach (User useremail in user)
            {
                using var smtp = new SmtpClient();

                email.To.Add(MailboxAddress.Parse(useremail.Email));
                email.Subject = "Birthday Notification";
                var builder = new BodyBuilder();
                var body = _ibirthDateService.GetUsersByDate(DateTime.Now.Month, DateTime.Now.AddMonths(11).Month);

                //Here the HTML part of the email from the Body property of the request
                builder.HtmlBody = string.Join(",\n", body);
                email.Body = builder.ToMessageBody();

                smtp.Connect(_mailSettings.Host, _mailSettings.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(_mailSettings.Mail, _mailSettings.Password);

                //Send the Message using the smpt’s SendMailAsync Method
                await smtp.SendAsync(email);
                smtp.Disconnect(true);
            }
        }
    }
}