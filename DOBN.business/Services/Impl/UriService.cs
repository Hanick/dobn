﻿using DOBN.business.Filter;
using Microsoft.AspNetCore.WebUtilities;
using System;

namespace DOBN.business.Services.Impl
{
    public class UriService : IUriService
    {
        private readonly string _baseUri;
        public UriService(string baseUri)
        {
            _baseUri = baseUri;
        }

        public Uri GetPageUri(PaginationFilter filter, string route)
        {
            //Makes a new Uri from base uri and route string (api.com + /api/customer = api.com/api/customer)
            var _enpointUri = new Uri(string.Concat(_baseUri, route));

            //Adds a new query string, “pageNumber”
            var modifiedUri = QueryHelpers.AddQueryString(_enpointUri.ToString(), "pageNumber", filter.PageNumber.ToString());

            //Similarly, we adds pageSize
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", filter.PageSize.ToString());

            return new Uri(modifiedUri);
        }
    }
}
