﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using BC = BCrypt.Net.BCrypt;
using DOBN.data.Entities;
using DOBN.data.Repositories;
using DOBN.sharing.DTOs.Response;
using DOBN.sharing.DTOs.Request;
using DOBN.sharing.DTOs.Models;
using DOBN.business.Helpers;

namespace DOBN.business.Services.Impl
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public UserService(IConfiguration configuration, 
            IMapper mapper,
            IUserRepository userRepository)
        {
       
            _userRepository = userRepository;
            _configuration = configuration;
            _mapper = mapper;
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = _userRepository
                .GetAll()
                .FirstOrDefault(x => x.UserName == model.UserName);

            if (user == null || !BC.Verify(model.Password, user.PasswordHash))
                return null;

            var token = _configuration.GenerateJwtToken(user);

            return new AuthenticateResponse(user, token);
        }

        public async Task<AuthenticateResponse> Register(UserModel userModel)
        {
            var user = _mapper.Map<User>(userModel);

            //Check username is existing
            var checkuser = _userRepository.GetAll().FirstOrDefault(x => x.UserName == user.UserName && x.Email == user.Email);

            if (checkuser != null)
                return null;

            //Registration, after registration will be Authentication
            var addedUser = await _userRepository.Add(new User
            {
                Id = user.Id,
                UserName = user.UserName,
                PasswordHash = BC.HashPassword(user.PasswordHash),
                Birthday = user.Birthday,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                IsSubscribed = user.IsSubscribed,
            }) ;

            var response = Authenticate(new AuthenticateRequest
            {
                UserName = user.UserName,
                Password = user.PasswordHash
            });

            return response;
        }

        public SubscriptionResponse GetSubscribed(SubscriptionRequest subscriptionRequest)
        {
            var user = _userRepository
                .GetAll()
                .FirstOrDefault(x => x.UserName == subscriptionRequest.UserName);

            if (user == null || !BC.Verify(subscriptionRequest.Password, user.PasswordHash))
                return null;

            user.IsSubscribed = subscriptionRequest.IsSubscribed;
            _userRepository.Update(user);

            return new SubscriptionResponse(user);
        }

        public IEnumerable<User> GetAll()
        {
            return _userRepository.GetAll();
        }

        public User GetById(int id)
        {
            return _userRepository.GetById(id);
        }
    }
}