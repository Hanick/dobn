﻿using DOBN.business.Filter;
using DOBN.data.Entities;
using DOBN.data.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DOBN.business.Services.Impl
{
    public class PagingService : IPagingService
    {
        private readonly IUserRepository _userRepository;
        private readonly ISortingService _sortingService;

        public PagingService(IUserRepository userRepository, ISortingService sortingService)
        {
            _userRepository = userRepository;
            _sortingService = sortingService;
        }

        public async Task<List<User>> PagedData(PaginationFilter filter)
        {
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
               //Call the sorting method
            var pagedData = await _sortingService.SortingData(filter)
               //Skips a certain set of records, by the page number * page size
               .Skip((validFilter.PageNumber - 1) * validFilter.PageSize)
               //Takes only the required amount of data, set by page size
               .Take(validFilter.PageSize)
               .ToListAsync();
            return pagedData;
        }

        public async Task<int> TotalRecords() 
        {
            //counts the total records for further use
            var totalRecords = await _userRepository.GetAll().CountAsync();
            return totalRecords;
        }

        public PaginationFilter ValidFilter(PaginationFilter filter)
        {
            var validFilter = new PaginationFilter(filter.PageNumber, filter.PageSize);
            return validFilter;
        }

    }
}
