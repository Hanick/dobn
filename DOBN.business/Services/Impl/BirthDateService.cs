﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using DOBN.data.Entities;
using DOBN.data.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using DOBN.sharing.DTOs.Response;

namespace DOBN.business.Services.Impl
{
    public class BirthDateService : IBirthDateService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public BirthDateService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public List<BirthDateResponse> GetUsersByDate(int frommonth, int tomonth)
        {
            //Value of frommonth must be greater than tomonth, it doesn't work any other way
            int from = frommonth;
            int to = tomonth;

            if (frommonth > tomonth)
            {
                from = tomonth;
                to = frommonth;
            }

            try
            {
                var user = _userRepository.GetAll().Where(x => x.Birthday.Month >= from &&  x.Birthday.Month <= to);
                var res = user.ProjectTo<BirthDateResponse>(_mapper.ConfigurationProvider).ToList();
                return res;
                
            }
            catch (NullReferenceException)
            {
                return null;
            }   
        }
    }
}
