﻿using DOBN.business.Filter;
using DOBN.data.Entities;
using DOBN.data.Repositories;
using System.Linq;

namespace DOBN.business.Services.Impl
{
    public class SortingService : ISortingService
    {
        private readonly IUserRepository _userRepository;

        public SortingService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IQueryable<User> SortingData(PaginationFilter filter)
        {
            var users = _userRepository.GetAll();

            if (filter.IsAscending)
            {
                users = filter.SortBy.ToLower() switch
                {
                    "firstname" => filter.IsAscending ? users.OrderBy(x => x.FirstName) : users.OrderByDescending(e => e.FirstName),
                    "lastname" => filter.IsAscending ? users.OrderBy(x => x.LastName) : users.OrderByDescending(e => e.LastName),
                    "username" => filter.IsAscending ? users.OrderBy(x => x.UserName) : users.OrderByDescending(e => e.UserName),
                    "birthday" => filter.IsAscending ? users.OrderBy(x => x.Birthday) : users.OrderByDescending(e => e.Birthday),
                    "email" => filter.IsAscending ? users.OrderBy(x => x.Email) : users.OrderByDescending(e => e.Email),
                    "issubscribed" => filter.IsAscending ? users.OrderBy(x => x.IsSubscribed) : users.OrderByDescending(e => e.IsSubscribed),
                    "id" => users.OrderBy(x => x.Id),
                    _ => users.OrderBy(x => x.Id),
                };
            }
            return users;
        }
    }
}
