﻿using DOBN.business.Filter;
using System;

namespace DOBN.business.Services
{
    public interface IUriService
    {
        public Uri GetPageUri(PaginationFilter filter, string route);
    }
}