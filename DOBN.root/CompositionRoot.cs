﻿using DOBN.business.Services;
using DOBN.business.Services.Impl;
using DOBN.business.Settings;
using DOBN.data;
using DOBN.data.Repositories;
using DOBN.data.Repositories.Impl;
using DOBN.sharing.Mapping;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DOBN.root
{
    public class CompositionRoot 
    {
        private readonly IConfiguration _configuration;
        public CompositionRoot(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void InjectDependencies(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddAutoMapper(typeof(UserProfile));

            services.AddTransient<IUserService, UserService>();
            services.AddScoped<IBirthDateService, BirthDateService>();
            services.AddScoped<IMailService, MailService>();
            services.AddScoped<IPagingService, PagingService>();
            services.AddScoped<ISortingService, SortingService>();
            services.AddScoped<IUriService, UriService>();

            services.AddHostedService<DateOfBirthAutoSender>();

            services.Configure<MailSettings>(_configuration.GetSection("MailSettings"));

            services.AddDbContext<DatabaseContext>(opt =>
                opt.UseNpgsql(_configuration.GetConnectionString("DefaultConnection")));

            services.AddHttpContextAccessor();

            services.AddSingleton<IUriService>(o =>
            {
                var accessor = o.GetRequiredService<IHttpContextAccessor>();
                var request = accessor.HttpContext.Request;
                var uri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());
                return new UriService(uri);
            });
        }
    }
}
