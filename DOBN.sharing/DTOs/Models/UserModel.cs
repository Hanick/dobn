﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DOBN.sharing.DTOs.Models
{
    public class UserModel
    {
        public long Id { get; set; }
        [Required]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} characters and no more than {1} characters", MinimumLength = 3)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} characters and no more than {1} characters", MinimumLength = 3)]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Required]
        [StringLength(16, ErrorMessage = "The {0} must be at least {2} characters and no more than {1} characters", MinimumLength = 3)]
        public string UserName { get; set; }
        [Required]
        [EmailAddress]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^((?=.*[a-z])(?=.*[A-Z])(?=.*\d)).+$")]
        [StringLength(18, ErrorMessage = "The {0} must be at least {2} characters and no more than {1} characters", MinimumLength = 6)]
        public string Password { get; set; }
        public bool IsSubscribed { get; set; }
    }
}
