﻿using System; 
using DOBN.data.Entities;

namespace DOBN.sharing.DTOs.Response
{
    public class AuthenticateResponse
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }

        public AuthenticateResponse(User user, string token)
        {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            Birthday = user.Birthday;
            UserName = user.UserName;
            Email = user.Email;
            Token = token;
        }
    }
}