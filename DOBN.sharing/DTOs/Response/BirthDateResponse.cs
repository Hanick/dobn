﻿using System;

namespace DOBN.sharing.DTOs.Response
{
    public class BirthDateResponse
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime Birthday { get; set; }

        public override string ToString()
        {
            return $"\t Name: {FirstName}" +
                $"\t LastName: {LastName}" +
                $"\t Email: {Email}" +
                $"\t Username: {UserName}" +
                $"\t BirthDate: {Birthday}" +
                $"\n" +
                $"\n";
        }
    }
}
