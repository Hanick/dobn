﻿using DOBN.data.Entities;

namespace DOBN.sharing.DTOs.Response
{
    public class SubscriptionResponse
    {
        public bool IsSubscribed { get; set; }

        public SubscriptionResponse(User user)
        {
            IsSubscribed = user.IsSubscribed;
        }
    }
}
