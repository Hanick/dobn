﻿using System.ComponentModel.DataAnnotations;

namespace DOBN.sharing.DTOs.Request
{
    public class AuthenticateRequest
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

    }
}