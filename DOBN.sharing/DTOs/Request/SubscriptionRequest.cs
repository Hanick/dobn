﻿namespace DOBN.sharing.DTOs.Request
{
    public class SubscriptionRequest : AuthenticateRequest
    {
        public bool IsSubscribed { get; set; }
    }
}
