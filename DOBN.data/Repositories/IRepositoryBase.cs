﻿using System.Linq;
using System.Threading.Tasks;
using DOBN.data.Entities;

namespace DOBN.data.Repositories
{
    public interface IRepositoryBase<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();
        T GetById(long id);
        Task<long> Add(T entity);
        Task<long> Update(T entity);
        Task<int> SaveChangesAsync();
    }
}