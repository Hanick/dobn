﻿using DOBN.data.Entities;

namespace DOBN.data.Repositories
{
    public interface IUserRepository : IRepositoryBase<User>
    {
        void DeleteUser(long id);
    }
}