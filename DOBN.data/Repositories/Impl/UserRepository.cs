﻿using DOBN.data.Entities;
using DOBN.data.Repositories.Impl;

namespace DOBN.data.Repositories
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(DatabaseContext context) : base(context)
        {

        }
        public void DeleteUser(long id)
        {
            var userentity = base.GetById(id);
            this._context.Remove(userentity);
        }
    }
}
