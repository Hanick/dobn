﻿using Microsoft.EntityFrameworkCore;
using DOBN.data.Entities;

namespace DOBN.data
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {

        }
        public DbSet<User> Users { get; set; }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }
    }
}