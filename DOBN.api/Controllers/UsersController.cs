﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DOBN.sharing.DTOs.Models;
using DOBN.business.Services;
using DOBN.sharing.DTOs.Request;
using DOBN.business.Helpers;

namespace DOBN.api.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _userService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password incorrect" });

            return Ok(response);
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserModel userModel)
        {
            var response = await _userService.Register(userModel);

            if (response == null)
                return BadRequest(new { message = "Didn't register!" });

            return Ok(response);
        }
        [HttpPost("Subscription")]
        public IActionResult GetSubscribed(SubscriptionRequest subscriptionRequest) 
        {
            var response = _userService.GetSubscribed(subscriptionRequest);

            if (response == null)
                return BadRequest(new { message = "Username or password incorrect!" });

            return Ok(response);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();

            return Ok(users);
        }
    }
}