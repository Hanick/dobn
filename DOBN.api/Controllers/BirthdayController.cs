﻿using DOBN.business.Services;
using Microsoft.AspNetCore.Mvc;

namespace DOBN.api.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class BirthdayController : ControllerBase
    {
        private readonly IBirthDateService _birthdateService;

        public BirthdayController(IBirthDateService birthdateService)
        {
            _birthdateService = birthdateService;
        }

        [HttpGet("BirthDate")]
        public IActionResult GetUsersByDate(int frommonth, int tomonth)
        {
            var response = _birthdateService.GetUsersByDate(frommonth, tomonth);

            if (response == null)
                return BadRequest(new { message = "There are no users, try another date" });

            return Ok(response);
        }
    }
}