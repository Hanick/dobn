﻿using DOBN.business.Filter;
using DOBN.business.Helpers;
using DOBN.business.Services;
using DOBN.business.Services.Impl;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace DOBN.api.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class FilterAndPagingController : ControllerBase
    {
        private readonly IPagingService _filterAndPagingService;
        private readonly IUriService _uriService;

        public FilterAndPagingController(IPagingService filterAndPagingService, IUriService uriService) 
        {
            _filterAndPagingService = filterAndPagingService;
            _uriService = uriService;
        }

        [HttpGet("Get Filter And Paging")]
        public async Task<IActionResult> GetUsers([FromQuery] PaginationFilter filter)
        {
            //route of the current controller action method
            var route = Request.Path.Value;

            var validFilter = _filterAndPagingService.ValidFilter(filter);
            var pagedData = await _filterAndPagingService.PagedData(filter);
            var totalRecords = await _filterAndPagingService.TotalRecords();

            //Wraps the paged data in PagedResponse Wrapper
            var pagedReponse = pagedData.CreatePagedReponse(validFilter, totalRecords, _uriService, route);
            return Ok(pagedReponse);
        }
    }
}
