﻿using DOBN.business.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DOBN.api.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class MailController : ControllerBase
    {
        private readonly IMailService _mailService;

        public MailController(IMailService mailService)
        { 
            _mailService = mailService; 
        }

        [HttpPost("Send")]
        public async Task<IActionResult> SendMail()
        {
            try
            {
                await _mailService.SendEmailAsync();
                return NoContent();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
